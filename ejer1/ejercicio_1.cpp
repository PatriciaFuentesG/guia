
#include <iostream>
using namespace std;

/* clases */
#include "Lista.h"
 int menu(){
     string opc;

     cout << "******MENU********" << endl;
     cout << "Agregar        [1]" << endl;
     cout << "Salir          [0]" << endl;
     cout << "******************" << endl;
     cout <<  "Opcion:";
     cin >> opc;
  
  return stoi(opc);
 }

/* función principal. */
int main (void) {
    // objeto lista.
   Lista *lista = new Lista();

    int opc = -1;
    int dato = 0;

    
    while (opc != 0){
   
        opc = menu();

         if(opc == 1){
            cout << "Valor: ";
            cin >> dato;
            lista->crear(dato);
            lista->imprimir();
         }
                

    }
    
    
    
    // libera memoria.
   delete lista;
  
  

    return 0;
}
