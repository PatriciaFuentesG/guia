# GUIA

Para poder usar esta aplicación usted debe tener IDE con leguaje c++ como Geany o Visual Studio Code, instalado en su computador.

Primero debe descargar todos los elementos de la carpeta. Abra la IDE y tendra tres carpetas cada una guarda un programa distinto.

Si abres la carpeta ejer1 habra un programa de números enteros y los vaya ingresando a una lista enlazada simple ordenada.Para ejecutarla escriba "make" y despues escriba:

         ./ejercicio_1 N -> Es la cantidad de numeros enteros con los que se trabajara.
Despues usted vera lo siguiente:
         
         ******MENU********
         Agregar        [1]->Al presionar este boton se le solicitara un numero y luego se ordenara en lista enlazada simple ordenada y se imprimira su version actual
         Salir          [0]->Al presionar este boton se saldra del programa
         ******************
         
Si abres la carpeta ejer2 habra programa que cree dos listas enlazadas ordenadas  y forme una tercera
lista que resulte de la mezcla de los elementos de ambas listas.Para ejecutarla escriba "make" y despues escriba:

         ./ejercicio_2 
Despues usted vera lo siguiente:

            ******MENU********
            Crear lista 1  [1]->Si escoje esta opcion se le solicitara un valor entero y despues se le consultara si desea seguir agregando datos en caso de que si presione 1 y en el caso contrario escriba 0
            Crear lista 2  [2]->Si escoje esta opcion se le solicitara un valor entero y despues se le consultara si desea seguir agregando datos en caso de que si presione 1 y en el caso contrario escriba 0
            Crear lista 3  [3]-> Se fusionaran las dos listas, y el resultado se imprimira
            Salir          [0]->Al presionar este boton se saldra del programa
            ******************
          
Si abres la carpeta ejer3 habra programa que con una lista valores no correlativos creada por el usuario , se rellenaran los espacios vacios en la lista  .Para ejecutarla escriba "make" y despues escriba:
            ./ejercicio_3
Despues usted vera lo siguiente:

            ******MENU********
            Agregar        [1]->Si escoje esta opcion se le solicitara un valor entero y despues se le consultara si desea seguir agregando datos en caso de que si presione 1 y en el caso contrario escriba 0
            Rellenar       [2]->Al presionar este boton se rellenara la lista y el resultado ser imprimira
            Salir          [0]->Al presionar este boton se saldra del programa
            ******************


 Eso es todo gracias por usar este programa
