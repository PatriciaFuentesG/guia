#include <iostream>
#include <cstring> 
using namespace std;

#include "Lista.h"
 int menu(){
     string opc;

     cout << "******MENU********" << endl;
     cout << "Agregar        [1]" << endl;
     cout << "Rellenar       [2]" << endl;
     cout << "Salir          [0]" << endl;
     cout << "******************" << endl;
     cout <<  "Opcion:";
     cin >> opc;
  
  return stoi(opc);
 }

/* función principal. */
int main (void) {
    // objeto lista.
   Lista *lista = new Lista();

    int opc = -1;
    int dato = 0;
    int salir = -1;
   
   

    
    while (opc != 0){
   
        opc = menu();

         if(opc == 1){
            while(salir != 0){
                cout << "Valor: ";
                cin >> dato;
                lista->crear(dato);
                lista->imprimir();
                cout << "Desea seguir agregado datos(en caso que no escriba 0, en el caso contrario escriba 1):"<<endl;
                cin >> salir;
            }
         }else if (opc == 2){
             lista->rellenar();
         }
                

    }
    
    
    
    // libera memoria.
   delete lista;
  
  

    return 0;
}
