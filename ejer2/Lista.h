#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;



/* define la estructura del nodo. */
typedef struct _Nodo {
    
    int numero = 0;
    struct _Nodo *sig;
} Nodo;

class Lista {
    private:
        Nodo *principio = NULL;
        Nodo *ultimo = NULL;
        Nodo *antes = NULL;
        Nodo *actual = NULL;
        Nodo *despues = NULL;
  

    public:
        /* constructor*/
        Lista();
        

        void crear (int numero);
   
        int imprimir ();

        bool orden(int numero);
        int arreglo(int n, int o);
        void unir(int numero);
};
#endif
