#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

//Esta funcion se encarga aparte de agregar elementos, es agregarlos en orden , buscando su ubicacion dentro de los nodos ya existentes

void Lista::crear (int numero) {
    Nodo *tmp;
    tmp = new Nodo;
    tmp->sig = NULL;
    tmp->numero = numero;
    bool mayor = false;
    this->actual = tmp;
    
           
    if (this->principio == NULL) { 
            
        this->principio = tmp;
        this->ultimo = this->principio;
  
    }else{  
        
             if(orden(numero) == false){
                
                 this->ultimo->sig = tmp;
                 this->ultimo = tmp;
             }else{
             
             tmp = this->principio;
             while(tmp != NULL && mayor == false) {
                 //cout << numero << " menor a" << tmp->numero<< endl; 
                 
                 if(numero < tmp->numero ){
                    /// cout << numero << " menor a" << tmp->numero<< endl; 
                     mayor = true;
                     this->despues = tmp;
                     break;
            
                }else{
                    // cout << numero << " mayor a" << tmp->numero<< endl; 
                      this->antes = tmp;  
                      tmp = tmp->sig;
                      //cout <<numero<< "siguiente valor "<< tmp->numero<< endl;
                     
                    }
    
             } 
             if(tmp == this->principio){
                 this->principio = this->actual;
                 this->actual->sig = tmp;
                 this->principio->sig = tmp;
             }else{
                
                 this->antes->sig = this->actual;
                 this->actual->sig = tmp;
              
             }     
            
         }       
   
  }
}

//Esta funcion sirve para saber si el nuevo elemento es mayor a los ya existentes
bool Lista::orden(int numero){
    Nodo *tmp = this->ultimo;
    bool posicion = false;
    if(numero < tmp->numero ){

         posicion = true;
           
    }
    
    return posicion;


}

int  Lista::imprimir () {
  
    Nodo *tmp = this->principio;

    cout << endl;
    int i =0;

    while (tmp != NULL) {
        cout << "numero:"<< tmp->numero << endl;
        tmp = tmp->sig; 
        i = i+1;
    }
    cout << endl;
   
    return i;
}
//En esta funcion se crea un arreglo , que facilitara crear una nueva lista fusionando las dos listas 
  int  Lista::arreglo(int n,int o){
    int arr[n];
    Nodo *tmp = this->principio;
    int i = 0;
    int p;
  

    while (tmp != NULL) {
        
        arr[i]= tmp->numero;
        tmp = tmp->sig; 
        i = i+1;

    }
    p = arr[o];
    return p;

  

  }